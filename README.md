# Helm chart for Lava

This is a helm chart for lava-server as used by Collabora.

## Usage

### Prerequisites

- Kubernetes cluster (e.g. [K3s](https://k3s.io))
- [Helm](https://helm.sh/docs/intro/install) binary

Example local environment can be found in [local-k3s](https://gitlab.collabora.com/pawiecz/local-k3s) repository.

### Deployment

Keep in mind secrets (`POSTGRES_PASSWORD` and `secretKey`) should be changed in your deployment.

Recommended value for `image.tag` might change in the future. For the most recent image tag check [`collabora-production` Container Registry](https://gitlab.collabora.com/lava/lava/container_registry/391).

```sh
git clone https://gitlab.collabora.com/lava/lava-helm-charts.git

export POSTGRES_PASSWORD='n0tS3crEt!'
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install --set auth.database=lavaserver --set auth.postgresPassword="$POSTGRES_PASSWORD" postgres bitnami/postgresql

envsubst < lava-helm-charts/overrides/db.yaml | helm install --set secretKey='als0#n0tS3crEt' --set image.tag="be5fd37c35fa6f653a6aaeb8bd19fe9164acbdd3" -f - lava lava-helm-charts/lava-server
```
